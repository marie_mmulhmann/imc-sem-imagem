package br.ifsc.edu.imcsemimagem;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText peso, altura;
    TextView resultadoIMC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void CalcularIMC(View v){

        peso = findViewById(R.id.pesoText);
        altura = findViewById(R.id.alturaText);
        resultadoIMC = findViewById(R.id.resultadoView);

        String PesoP = peso.getText().toString();
        String AlturaA = altura.getText().toString();

        float finalPeso = Float.parseFloat(PesoP);
        float finalAltura = Float.parseFloat(AlturaA);

        float IMC = (finalPeso/(finalAltura*finalAltura));

        String resultadIMC = String.valueOf(IMC);
        resultadoIMC.setText(resultadIMC);


        if (IMC>=39){
            Toast toast = Toast.makeText(getApplicationContext(), "Obesidade mórbida", Toast.LENGTH_LONG);
            toast.show();
        }else if(IMC<=38.9 || IMC>=29){
            Toast toast = Toast.makeText(getApplicationContext(), "Obesidade moderada", Toast.LENGTH_LONG);
            toast.show();
        }else if (IMC>19 || IMC<=28.9){
            Toast toast = Toast.makeText(getApplicationContext(), "Obesidade leve", Toast.LENGTH_LONG);
            toast.show();
        }else if (IMC<19 || IMC<=23.9){
            Toast toast = Toast.makeText(getApplicationContext(), "Peso Ideal", Toast.LENGTH_LONG);
            toast.show();
        }
        else if (IMC<=18){
            Toast toast = Toast.makeText(getApplicationContext(), "Abaixo do peso", Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
